$(document).ready(function () {
    $("#sendRequest").click(sendRequest);
    $("#saveConfig").click(saveConfig);
    $('#sendRequestLoading').hide();
    let CONFIGURATION = {
        api_key: "",
        api_url: "",
        token: "",
        token_header_name: "",
        is_configured: false
    }
    function sendRequest() {
        toggleLoading();
        $.ajax({
            url: CONFIGURATION.api_url,
            contentType: "application/json", type: 'POST',
            headers: {
                "x-api-key": CONFIGURATION.api_key,
                [CONFIGURATION.token_header_name]: CONFIGURATION.token
            },
            data: JSON.stringify({
                query: `${$("#request").val()}`
            }),
            success: function (result) {
                $('#response').html(JSON.stringify(result, null, 4));
                toggleLoading();
            },
            error: function (err) {
                $('#response').html(JSON.stringify(JSON.parse(err.responseText), null, 4));
                toggleLoading();
            }
        });
    }

    function toggleLoading(){
        $('#sendRequestText').toggle();
        $('#sendRequestLoading').toggle();
    }

    function saveConfig() {
        localStorage.setItem("configuration.api_key", $('input[name="api_key"]').val());
        localStorage.setItem("configuration.api_url", $('input[name="api_url"]').val())
        localStorage.setItem("configuration.token", $('input[name="token"]').val());
        localStorage.setItem("configuration.token_header_name", $('input[name="token_header_name"]').val());
        localStorage.setItem("configuration.is_configured", true)
        loadFromStorage();
        $('#configureModal').modal('hide')
    };

    function loadFromStorage() {
        if (localStorage.getItem("configuration.is_configured")) {
            CONFIGURATION.api_key = localStorage.getItem("configuration.api_key");
            CONFIGURATION.api_url = localStorage.getItem("configuration.api_url");
            CONFIGURATION.token = localStorage.getItem("configuration.token");
            CONFIGURATION.token_header_name = localStorage.getItem("configuration.token_header_name");
            CONFIGURATION.is_configured = true;
        }
    }

    function updateView(){
        $('input[name="api_key"]').val(CONFIGURATION.api_key);
        $('input[name="api_url"]').val(CONFIGURATION.api_url);
        $('input[name="token"]').val(CONFIGURATION.token);
        $('input[name="token_header_name"]').val(CONFIGURATION.token_header_name);
    }
    loadFromStorage();
    updateView();
});