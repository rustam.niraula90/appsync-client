# Appsync Client
 A simple jquery based client for aws appsync.

 ## Requirement

[npm](https://www.npmjs.com/)

[http-server](https://www.npmjs.com/package/http-server) 

 ## Installation

 **Install** the dependencies

```bash
npm install
```

**Run** using :

```bash
http-server
```
